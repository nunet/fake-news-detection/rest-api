from aiohttp import web
import json
import os
import time
import threading
from time import sleep
from utils import *
import log
import opentelemetry
from opentelemetry import trace
from opentelemetry.instrumentation.grpc import GrpcInstrumentorServer
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.exporter.jaeger.thrift import JaegerExporter
from opentelemetry.sdk.trace.export import BatchSpanProcessor
from opentelemetry.instrumentation.grpc import GrpcInstrumentorClient
from ipv8.community import Community
from ipv8.configuration import ConfigBuilder, Strategy, WalkerDefinition, default_bootstrap_defs
from ipv8_service import IPv8
from ipv8.lazy_community import lazy_wrapper
import ipv8_messages
from asyncio import ensure_future, get_event_loop
import asyncio



jaeger_address = os.environ['jaeger_address']


jaeger_exporter = JaegerExporter(
    agent_host_name=jaeger_address,
    agent_port=6831,
)

trace_provider=trace.get_tracer_provider().add_span_processor(
    BatchSpanProcessor(jaeger_exporter)
)

trace.set_tracer_provider(TracerProvider())
instrumentor = GrpcInstrumentorClient().instrument()


tracer_client=opentelemetry.instrumentation.grpc.client_interceptor(tracer_provider=trace_provider)
tracer=trace.get_tracer(__name__)


current_span = trace.get_current_span()

current_span.set_attribute("http.route", "some_route")

logger = log.setup_custom_logger('TRACE')

routes = web.RouteTableDef()

class Ipv8(Community):
    community_id = b'\xc2\x98da\x0b\xa7LN\x8a(\xc1\xfcV\xf7m0\xc5s\xc2\x99'

    def __init__(self,my_peer,endpoint,network,max_peers=100):
        super().__init__(my_peer,endpoint,network)
        self.max_peers = max_peers
        self._lock = threading.RLock()
        self.reqService_result = {}
        self.add_message_handler(ipv8_messages.reqServiceResultRestApi,self.reqService_result_handler)
         
    
    @lazy_wrapper(ipv8_messages.reqServiceResultRestApi)
    def reqService_result_handler(self,peer,payload):
        logger.info("Inside reqService_result_handler")
        msg = decode_message(payload.result)
        call_id = msg["call_id"]
        logger.info(f"Received result from peer: {peer}")
        # logger.info(msg["response"])
        logger.info(msg)
        with self._lock:
            self.reqService_result[call_id] = msg

    def started(self):
        async def start_communication():
           for p in self.get_peers():
               logger.info(f"I am --- {self.my_peer}")
               logger.info(f"No of peers = {len(self.get_peers())}")
               logger.info(p)
        # self.register_task("start_communication",start_communication,interval=5.0,delay=0) 


class RestApi():
    def __init__(self):
        self.overlay = None

    async def start_overlay(self):
        builder = ConfigBuilder().clear_keys().clear_overlays()
        builder.add_key("my peer", "medium", "key.pem")
        builder.add_overlay("Ipv8", "my peer", [WalkerDefinition(Strategy.RandomWalk, 10, {'timeout': 3.0})],
                            default_bootstrap_defs, {}, [])
        ipv8 = IPv8(builder.finalize(), extra_communities={'Ipv8': Ipv8})
        await ipv8.start()
        self.overlay = ipv8.overlays[0]
        print("Started overlay")

    async def run(self):
        @routes.get('/get_score')
        async def get_score(request):
            logger.info(self.overlay.my_peer)
            logger.info(request.query['url'])
            url = request.query["url"]
            logger.info(request)
            with tracer.start_as_current_span("client_span") as span:
                span.add_event("request url", {"request": url})
                trace_info=span.get_span_context()
                try:
                    try:
                        span_id = trace_info.span_id
                        trace_id = trace_info.trace_id
                        encoded_msg,peer,call_id = call_ipv8(url,trace_id,span_id)
                        self.overlay.reqService_result[call_id] = None
                        self.overlay.ez_send(peer,ipv8_messages.reqServiceMsg(encoded_msg))
                        start_time = time.time()
                        timeout = 20
                        while self.overlay.reqService_result[call_id] == None:
                            if time.time() > start_time + timeout:
                                logger.info(f"IPv8 call duration == {time.time() - start_time}")
                                raise Exception("IPv8 communication timed out")
                            await asyncio.sleep(1/1000)
                        logger.info(f"IPv8 call duration == {time.time() - start_time}")
                        result = self.overlay.reqService_result[call_id]
                    except Exception as e:
                        logger.error("IPv8 communication timed out. Trying gRPC.")
                        start = time.time()
                        result = await call_grpc(url)
                        logger.info(f"gRPC time == {time.time() - start}")
                    trace_id=str(trace_info)[23:]
                    trace_id=str(str(trace_id)[:32])
                    final_result={"call":"","call_id":"","trace_id":trace_id}
                    return_code=result["return_code"]
                    if return_code==1:
                        exception = Exception("Empty body or headline")
                        span.record_exception(exception)
                        return web.Response(body=json.dumps(final_result),status= 400)
                    result,call_id = json.loads(result["response"]),result["call_id"]
                    result=result["response"]
                    result=eval(result)
                    result=result["call"]
                    result["url"]=url
                    result["return_code"] = return_code
                    final_result["call"]=str(result)
                    final_result["call_id"]=str(call_id)
                except Exception as e:
                    logger.error(e)
                    add_failed_call(call_id)
                    exception = Exception(str(e))
                    span.record_exception(exception)
                    span.set_status(Status(StatusCode.ERROR, "error occured"))

                    return web.Response(body=json.dumps(final_result),status= 500)
                span.add_event("request url", {"request": str(final_result)})
                logger.info("trace info:"+str(trace_info))
                logger.info(final_result)
            return web.Response(body=json.dumps(final_result))

        @routes.get('/get_escrow')
        async def get_escrow(request):
            call_id = request.query['call_id']
            logger.info(request)
            sleep(20 / 1000)
            with tracer.start_as_current_span("client_span") as span:
                span.add_event("request call id", {"request": call_id})
                try:
                    result = get_escrow_from_db(call_id)
                except Exception as e:
                    logger.error(e)
                    exception = Exception(str(e))
                    span.record_exception(exception)
                span.add_event("escrow address", {"escrow address": str(result.escrow_address)})
                logger.info(result)
            return web.Response(body=result.escrow_address)

        @routes.get('/get_result')
        async def get_result(request):
            call_id = request.query['call_id']
            logger.info(request)
            sleep(20 / 1000)
            with tracer.start_as_current_span("client_span") as span:
                span.add_event("request call id", {"request": call_id})
                try:
                    result = get_result_from_db(call_id)
                except Exception as e:
                    logger.error(e)
                    exception = Exception(str(e))
                    span.record_exception(exception)
                span.add_event("result ", {"result": str(result.result)})
                logger.info(result)
            result=eval(result.result)
            final_result=[]
            for item in result:
                final_result.append(eval(item))

            return web.Response(body=str(final_result))

        @routes.get('/get_txn')
        async def get_txn(request):
            call_id = request.query['call_id']
            logger.info(request)
            sleep(20 / 1000)
            with tracer.start_as_current_span("client_span") as span:
                span.add_event("request call id", {"request": call_id})
                try:
                    result = get_txn_hash_from_db(call_id)
                except Exception as e:
                    logger.error(e)
                    exception = Exception(str(e))
                    span.record_exception(exception)
                span.add_event("transaction hash ", {"transaction hash": str(result.txn_hash)})
                logger.info(result)
            return web.Response(body=result.txn_hash)
        
        @routes.get('/get_agi_txn')
        async def get_agi_txn(request):
            call_id = request.query['call_id']
            logger.info(request)
            sleep(20 / 1000)
            with tracer.start_as_current_span("client_span") as span:
                span.add_event("request call id", {"request": call_id})
                try:
                    result = get_agi_txn_hash_from_db(call_id)
                except Exception as e:
                    logger.error(e)
                    exception = Exception(str(e))
                    span.record_exception(exception)
                span.add_event("transaction hash ", {"transaction hash": str(result.txn_hash)})
                logger.info(result)
            return web.Response(body=result.txn_hash)
        
        app = web.Application()
        app.add_routes(routes)
        runner = web.AppRunner(app, access_log=None)
        await runner.setup()
        self.site = web.TCPSite(
            runner, '0.0.0.0', os.getenv("API_PORT"))
        await self.site.start()
        print("Started API")


def main():
    restApi = RestApi()
    loop = get_event_loop()
    ensure_future(restApi.start_overlay())
    ensure_future(restApi.run())

    loop.run_forever()


if __name__ == '__main__':
    main()
