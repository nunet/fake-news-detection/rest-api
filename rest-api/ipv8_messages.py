from ipv8.messaging.lazy_payload import VariablePayload, vp_compile
from ipv8.lazy_community import lazy_wrapper

@vp_compile
class reqServiceMsg(VariablePayload):
    msg_id = 5
    format_list = ['raw']
    names = ["parameters"]

@vp_compile
class reqServiceResultRestApi(VariablePayload):
    msg_id = 9
    format_list = ['raw']
    names = ['result']
