# REST API

# Steps To Run


## Build
``` 
docker-compose build rest_api
```
## Start
``` 
docker-compose up rest_api
```


# Test

## In a new terminal
``` 
curl demo.nunet.io:7006/get_score?url=https://bbc.com
```