import sys
import grpc
import os
import log
import pickle
from extract import *

from service_spec import nunet_adapter_pb2
from service_spec import nunet_adapter_pb2_grpc
from service_spec import service_pb2
from service_spec import service_pb2_grpc
from service_spec import stats_db_pb2 as pb2
from service_spec import stats_db_pb2_grpc as pb2_grpc

from ipv8.peer import Peer
from ipv8.keyvault.public.libnaclkey import LibNaCLPK

import re
import ipv8_messages


deployment_type = os.environ['deployment_type']

registry_address = os.environ['registry_address']

stats_db_address = os.environ['stats_db_address']


if deployment_type=="prod":
    registry_port="4558"
    service_name="news-score"
else:
    service_name="testing-news-score"
    registry_port="4658"

logger = log.setup_custom_logger('TRACE')

def preprocessing(headline,body):
    # CURRENTLY WE ARE CHECKING IF THE HEADLNE AND BODY ARE NOT EMPTY 
    # WE CAN ADD OTHER CHECKS TOO 
    result = {}

    headline=re.escape(headline)
    body = re.escape(body)

    body=body.replace("\'","\\\'").replace("\"","\\\"")
    body=body.replace("\n"," ").replace("\t"," ")
    body=body.replace("\b"," ").replace("\r"," ")
    body=body.replace("\f"," ")
    
    headline=headline.replace("\'","\\\'").replace("\"","\\\"")
    headline=headline.replace("\n"," ").replace("\t"," ")
    headline=headline.replace("\b"," ").replace("\r"," ")
    headline=headline.replace("\f"," ")

    if (headline.strip()=='') or (body.strip() == ''):
        result["return_code"] = 1
    else:
        result["return_code"] = 0

    result["headline"] = headline
    result["body"] = body

    return result,headline,body 

def get_address():
    with grpc.insecure_channel(str(registry_address)+':'+str(registry_port)) as channel:
        stub = service_pb2_grpc.RegistryStub(channel)
        response = stub.reqServiceEndpoints(
            service_pb2.ServiceRequest(service_name=service_name))
        endpoints = pickle.loads(response.endpoints)
        peer_ids = endpoints["peer_ids"]
        ip_addrs = endpoints["endpoints"]
        if len(peer_ids) > 0:
            peer_info = peer_ids[len(peer_ids) - 1]
            key = LibNaCLPK(peer_info['key'])
            peer = Peer(key)
            peer.mid = peer_info['mid']
            peer.public_key = peer_info['public_key']
            peer._address = peer_info['_address']
        else:
            peer = ""
        
        address = ip_addrs[len(ip_addrs)-1][0]
        port = ip_addrs[len(ip_addrs)-1][1]
    return peer, (str(address), str(port))

async def call_grpc(url):
    req_address=get_address()
    peer = req_address[0]
    address=req_address[1][0]
    port=req_address[1][1]
    adapter_address = address+":"+port
    headline = extract_headline(url)
    body = extract_body(url)
    
    result,headline,body = preprocessing(headline=headline,body=body)
    
    channel = grpc.insecure_channel("{}".format(stats_db_address))
    stub = pb2_grpc.StatsDatabaseStub(channel)
    call_id=stub.add_call(pb2.DatabaseAddCallInput(url=url))
    call_id=call_id.call_id
    if result["return_code"] == 0:
        channel = grpc.aio.insecure_channel("{}".format(adapter_address))
        stub = nunet_adapter_pb2_grpc.NunetAdapterStub(channel)
        fp = nunet_adapter_pb2.ServiceDefnition()
        fp.service_name = service_name
        fp.params = '{"body":"'+body+'","headline":"'+headline+'","call_id":"'+str(call_id)+'"}'
        response = await stub.reqService(fp)
        print(response)
        return {"response":response.service_response,"return_code":result["return_code"],"call_id":str(call_id)}
       
    else:
        return {"response":None,"return_code":result["return_code"]}

def call_ipv8(url,trace_id,span_id):
    req_address=get_address()
    peer = req_address[0]
    address=req_address[1][0]
    port=req_address[1][1]
    adapter_address = address+":"+port
    headline = extract_headline(url)
    body = extract_body(url)
    
    result,headline,body = preprocessing(headline=headline,body=body)

    channel = grpc.insecure_channel("{}".format(stats_db_address))
    stub = pb2_grpc.StatsDatabaseStub(channel)
    call_id=stub.add_call(pb2.DatabaseAddCallInput(url=url))
    call_id=call_id.call_id

    msg = {}
    msg["headline"] = headline
    msg["service_name"] = service_name
    msg["body"] = body
    msg["call_id"] = call_id
    msg["result"] = result
    msg['trace_id'] = trace_id
    msg['span_id'] = span_id
    encoded_result = encode_message(msg)
    return encoded_result,peer,call_id

def get_escrow_from_db(call_id):    
    channel = grpc.insecure_channel("{}".format(stats_db_address))
    stub = pb2_grpc.StatsDatabaseStub(channel)
    result=stub.get_escrow(pb2.DatabaseGetEscrowInput(call_id=str(call_id)))
    print(result)
    return result
    
def get_result_from_db(call_id):    
    channel = grpc.insecure_channel("{}".format(stats_db_address))
    stub = pb2_grpc.StatsDatabaseStub(channel)
    result=stub.get_result(pb2.DatabaseGetResultInput(call_id=str(call_id)))
    return result

def get_txn_hash_from_db(call_id):    
    channel = grpc.insecure_channel("{}".format(stats_db_address))
    stub = pb2_grpc.StatsDatabaseStub(channel)
    result=stub.get_txn(pb2.DatabaseGetTransactionInput(call_id=str(call_id)))
    return result

def encode_message(data):
    return pickle.dumps(data)

def decode_message(payload):
    return pickle.loads(payload)
def get_agi_txn_hash_from_db(call_id):    
    channel = grpc.insecure_channel("{}".format(stats_db_address))
    stub = pb2_grpc.StatsDatabaseStub(channel)
    result=stub.get_agi_txn(pb2.DatabaseGetTransactionInput(call_id=str(call_id)))
    return result

def add_failed_call(call_id):    
    channel = grpc.insecure_channel("{}".format(stats_db_address))
    stub = pb2_grpc.StatsDatabaseStub(channel)
    stub.failed_calls(pb2.DatabaseFailedCallInput(call_id=str(call_id)))
